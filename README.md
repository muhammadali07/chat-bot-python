# Chat Bot Python Flask

## Initial Setup:

Cloning dan buat virtual enviroment
```
$ git clone https://gitlab.com/muhammadali07/chat-bot-python.git
$ cd chat-bot-python
$ python3 -m venv venv
$ . venv/bin/activate
```
Install Library yang dibutuhkan
```
$ (venv) pip install Flask torch torchvision nltk
```
Install nltk package
```
$ (venv) python
>>> import nltk
>>> nltk.download('punkt')
```
Ubah `intents.json` dengan pertanyaan dan jawaba sesuai dengan keinginan kita

Run
```
$ (venv) python train.py
```
Ini akan menghasilkan file data.path, 
```
$ (venv) python chat.py
```
$ (venv) python app.py

Jalankan perintah tersebut untuk menjalankan aplikasi tersebut, lalu buka browser dan akses :
http://127.0.0.1:5000

Atau kita dapat menjalankan via Docker,.

$ cd chat-bot-python
$ docker-compose -f docker-compose.yml up -d

tunggu hingga prosesnya selesai, kemudian akses sesuai alamat container.