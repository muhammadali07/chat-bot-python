from flask import Flask, redirect, render_template, request, jsonify, url_for

from functools import wraps

from chat import get_response

from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'super-secret'

auth = HTTPBasicAuth()

users = {
    "john": generate_password_hash('secret'),
    "admin" : generate_password_hash('admin')
}

@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username

@app.get("/")
@auth.login_required
def index():
    return redirect(url_for('chatbot'))

@app.get("/chatbot")
@auth.login_required
def chatbot():
    return render_template("chat.html")

@app.post("/predict")
def predict():
    text = request.get_json().get("message")
    response = get_response(text)
    message = {"answer" : response}
    return jsonify(message)


if __name__ =="__main__":
    app.run(debug=True)
